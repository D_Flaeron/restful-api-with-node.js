const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

exports.user_get_all_users = (req, res, next) => {
  User.find()
    .exec()
    .then(users => {
      const response = {
        count: users.length,
        users: users.map(item => {
          return {
            _id: item._id,
            email: item.email,
          }
        }),
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log('Error', err);
      res.status(500).json({
        error: err
      });
    })
};

exports.user_signup = (req, res, next) => {
  User.find({email: req.body.email})
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return res.status(409).json({
          massage: 'Mail exist'
        })
      } else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            res.status(500).json({
              error: err
            })
          } else {
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              email: req.body.email,
              password: hash
            });

            user
              .save()
              .then(result => {
                console.log('user', result);
                res.status(201).json({
                  message: 'User created'
                })
              })
              .catch(err => {
                console.log('Error', err);
                res.status(500).json({
                  error: err
                })
              })
          }
        });
      }
    });
};

exports.user_login = (req, res, next) => {
  User.find({email: req.body.email})
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: 'Auth failed'
        });
      } else {
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
          if (err) {
            return res.status(401).json({
              message: 'Auth failed'
            });
          }

          if (result) {
            const token = jwt.sign({
                email: user[0].email,
                userId: user[0]._id
              },
              process.env.JWT_KEY,
              {
                expiresIn: '1h'
              }
            );

            return res.status(200).json({
              message: 'Auth successful',
              token: token,
              userId: user[0]._id,
              email: user[0].email
            })
          }

          return res.status(401).json({
            message: 'Auth failed'
          });
        });
      }
    })
    .catch(err => {
      console.log('Error', err);
      res.status(500).json({
        error: err
      })
    })
};

exports.user_delete = (req, res, next) => {
  User.find({_id: req.params.userId})
    .exec()
    .then(user => {
      if (user.length >= 1) {
        User.remove({_id: req.params.userId})
          .exec()
          .then(response => {
            res.status(200).json({
              message: 'User deleted'
            })
          })
          .catch(err => {
            res.status(500).json({
              error: err
            })
          })
      } else {
        res.status(500).json({
          error: 'User id not found'
        })
      }
    })
    .catch(err => {
      res.status(500).json({
        error: err
      })
    });


};