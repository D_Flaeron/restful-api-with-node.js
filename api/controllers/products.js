const mongoose = require('mongoose');

const Product = require('../models/product');

exports.products_get_all = (req, res, next) => {
  Product.find()
    .select('_id name price productImage')
    .exec()
    .then(products => {
      const response = {
        count: products.length,
        products: products.map(item => {
          return {
            _id: item._id,
            name: item.name,
            price: item.price,
            productImage: item.productImage
          }
        }),
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log('err', err);
      res.status(500).json({err: err})
    });
};

exports.products_create_product = (req, res, next) => {
  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    price: req.body.price,
    productImage: req.file.path
  });

  product
    .save()
    .then(result => {
      res.status(201).json({
        message: 'Created product successfully',
        createdProduct: {
          _id: result._id,
          name: result.name,
          price: result.price
        }
      });
    }).catch(err => {
    console.log('err', err);
    res.status(500).json({err: err})
  });
};

exports.products_get_product = (req, res, next) => {
  const id = req.params.productId;

  Product.findById(id)
    .exec()
    .then(product => {
      if (product) {
        res.status(200).json({
          _id: product._id,
          name: product.name,
          price: product.price,
        })
      } else {
        res.status(404).json({message: 'No valid result'})
      }
    })
    .catch(err => {
      console.log('err', err);
      res.status(500).json({
        err: err
      })
    })
};

exports.products_update_product = (req, res, next) => {
  const id = req.params.productId;
  const updateOps = {};

  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value
  }

  Product.update({_id: id}, {$set: updateOps})
    .exec()
    .then(result => {
      res.status(200).json({
        message: 'Product updated'
      });
    })
    .catch(err => {
      console.log('Error', err);
      res.status(500).json({error: err});
    })
};

exports.products_delete_product = (req, res, next) => {
  const id = req.params.productId;

  Product.remove({_id: id})
    .exec()
    .then(result => {
      res.status(200).json({
        message: 'Product deleted'
      });
    })
    .catch(err => {
      console.log('err', err);
      res.status(500).json({
        error: err
      });
    });
};